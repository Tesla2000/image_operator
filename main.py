import subprocess
import time
from random import randint

import pytesseract
import cv2
import numpy as np


class ImageOperator:
  @staticmethod
  def _convert_string_to_image(function):
    def inner(*args, **kwargs):
      args = tuple(map(lambda argument: cv2.imread(argument) if isinstance(argument, str) else argument, args))
      return function(*args, **kwargs)
    return inner


  @staticmethod
  @_convert_string_to_image
  def locate_image_on_image(image_to_be_found, image_to_look_on, minimal_accuracy=0.8, offset_x=0,
                            offset_y=0, centered=False, randomize=False):
    result = cv2.matchTemplate(image_to_be_found, image_to_look_on, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    if max_val < minimal_accuracy:
      return False
    return ImageOperator._apply_position_changes(max_loc[0], max_loc[1], image_to_be_found, offset_x, offset_y,
                                                 centered, randomize)

  @staticmethod
  @_convert_string_to_image
  def _apply_position_changes(x, y, image=None, offset_x=0, offset_y=0, centered=False, randomize=False):
    x, y = x + offset_x, y + offset_y
    height, width, _ = image.shape
    if randomize and not centered:
      raise ValueError("If coordinates are to be randomized location must centered")
    if centered or randomize:
      x, y = x + width // 2, y + height // 2
    if randomize:
      x, y = x + randint(-width // 2 + 1, width // 2 - 1), \
             y + randint(-height // 2 + 1, height // 2 - 1)
    return x, y

  @staticmethod
  def _is_far_enough(coordinate, coordinates, limit=10):
    return min((
      *map(lambda countour2: ((coordinate[0] - countour2[0]) ** 2 + (coordinate[1] - countour2[1]) ** 2) ** .5,
           coordinates), limit + 1)) > limit

  @staticmethod
  @_convert_string_to_image
  def locate_all_images_on_image(image_to_be_found, image_to_look_on, minimal_accuracy=0.9, from_bottom=True,
                                 offset_x=0, offset_y=0, centered=False, randomize=False):
    result = cv2.matchTemplate(image_to_be_found, image_to_look_on, cv2.TM_CCOEFF_NORMED)
    coordinates = np.transpose(np.where(result > minimal_accuracy)[::-1], (1, 0))
    coordinates = tuple(map(tuple, coordinates))
    coordinates = tuple(map(
        lambda coordinate: ImageOperator._apply_position_changes(coordinate[0], coordinate[1], image_to_look_on,
                                                                   offset_x, offset_y, centered, randomize),
          filter(
            lambda coordinate: ImageOperator._is_far_enough(coordinate, coordinates[:coordinates.index(coordinate)]),
            coordinates)))
    return coordinates if not from_bottom else coordinates[::-1]

  @staticmethod
  @_convert_string_to_image
  def wait_for_image(image_to_be_found, image_to_look_on_generator, minimal_accuracy=0.8, wait_for=float('inf'),
                     offset_x=0, offset_y=0, centered=False, randomize=False):
    start = time.time()
    while time.time() - wait_for < start:
      image_to_look_on = image_to_look_on_generator()
      if coordinate := ImageOperator.locate_image_on_image(image_to_be_found, image_to_look_on, minimal_accuracy,
                                                            offset_x, offset_y, centered, randomize):
        return coordinate
    return False

  @staticmethod
  @_convert_string_to_image
  def get_text_from_image(image, lang='pol', config='--psm 11'):
    return pytesseract.image_to_string(image, lang=lang, config=config)

  @staticmethod
  @_convert_string_to_image
  def show_images_on_image(image_to_be_found, image_to_look_on, minimal_accuracy=0.9, from_bottom=True, offset_x=0,
                           offset_y=0, centered=False, randomize=False, radius=10, color=(0, 0, 0), show=True):
    for coordinate in ImageOperator.locate_all_images_on_image(image_to_be_found, image_to_look_on, minimal_accuracy,
                                                               from_bottom, offset_x, offset_y, centered, randomize):
      image_to_look_on = cv2.circle(image_to_look_on, coordinate, radius, color, -1)
    if show:
      cv2.imshow('image', image_to_look_on)
      cv2.waitKey(0)
    return image_to_look_on

